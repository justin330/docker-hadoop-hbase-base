# Apache Hadoop and Apache HBase Base Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-hadoop-hbase-base/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-hadoop-hbase-base/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-hadoop-hbase-base/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-hadoop-hbase-base/commits/master)

The image is based on [rychly-docker/docker-hadoop-base](/rychly-edu/docker/docker-hadoop-base).
The version of the base image can be restricted on build by the `HADOOP_VERSION` build argument.

The image is utilize another image on [rychly-docker/docker-hbase](/rychly-edu/docker/docker-hbase).
The version of the utilized image can be restricted on build by the `HBASE_VERSION` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-hadoop-hbase-base:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-hadoop-hbase-base" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Configuration

Both Hadoop and HBase configuration can be reset by environment variables with prefixes (see the corresponding source images mentioned above):

*	`PROP_CORE`, `PROP_HDFS`, `PROP_YARN`, `PROP_MAPRED`, `PROP_HTTPFS` and `PROP_KMS` for Hadoop
*	`PROP_SITE` and `PROP_POLICY` for HBase

Names of the environment variables are transformed into names of the properties by replacements

*	`___ => -`
*	`__ => _`
*	`_ => .`
